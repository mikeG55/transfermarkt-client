from setuptools import setup, find_packages

VERSION = '0.0.1'


def long_description():
    """ Get description from readme """
    with open("README.md", "r") as fpath:
        long_desc = fpath.read()
    return long_desc


# Setting up
setup(
    name="transfermarkt",
    version=VERSION,
    description="An API for scraping data from transfermarkt.com",
    packages=find_packages(exclude=("tests",)),
    long_description=long_description(),
    long_description_content_type="text/markdown",
    author="Mike Hayward",
    author_email="mike_hayward@hotmail.co.uk",
    license="MIT",
    install_requires=[],

    keywords=['python', 'transfermarkt', 'api', 'scraper',
              'web scraper'],
    project_urls={
        "Source": "https://gitlab.com/mikeG55/transfermarkt-client/",
    },

)

