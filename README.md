# TransfermarketClient

This is a python API for scraping data from transfermarkt.com. Transfermarkt is a website with football data for a number of competitions. The continents currently available are the 'europa', 'asien', 'amerika', and 'afrika'. 

This package is not associated with transfermarkt.com

---
### Installation

---

### Getting Started

---
**Please Note**

This package is still in the earlier stages of development.


## TODO:
* unittests
* hook up getting squads
* hook up getting squad stats
* hook up player profiles
* hook up player transfers
* hook up player absences
* hook up player market values
* hook up search functionality

```
from transfermarkt import TransfermarktClient

transfermarkt = TransferMarktClient()
```

get available continents
```
continents = transfermarkt.continents().get()

['europa', 'asien', 'amerika', 'afrika']
```

pass in a continent to get available countries for the continent 
```
countries = transfermarkt.countries('europa').get()

[   
    {"id": "189", "name": "England"}, 
    {"id": "75", "name": "Italy"}, 
    {"id": "157", "name": "Spain"}, 
    {"id": "40", "name": "Germany"}, 
    {"id": "50", "name": "France"}
] 
```

pass in a country_id and season to get competitions available for the country

```
competitions = transfermarkt.competitions(189, 2020).get()

[
    {'id': 'GB1', 'name': 'Premier League', 'slug': 'premier-league'}, 
    {'id': 'GB2', 'name': 'Championship', 'slug': 'championship'}, 
    {'id': 'GB3', 'name': 'League One', 'slug': 'league-one'}, 
    {'id': 'GB4', 'name': 'League Two', 'slug': 'league-two'}, 
    {'id': 'CNAT', 'name': 'National League', 'slug': 'national-league'}
]

```

pass in a competition_id and season to get teams available for the competition

```
teams = transfermarkt.teams('GB1', 2020).get()

[
    {'id': '281', 'name': 'Man City', 'slug': 'manchester-city'}, 
    {'id': '31', 'name': 'Liverpool', 'slug': 'fc-liverpool'}, 
    {'id': '631', 'name': 'Chelsea', 'slug': 'fc-chelsea'}, 
    {'id': '985', 'name': 'Man Utd', 'slug': 'manchester-united'}, 
    {'id': '148', 'name': 'Spurs', 'slug': 'tottenham-hotspur'}, 
    {'id': '11', 'name': 'Arsenal', 'slug': 'fc-arsenal'}, 
    {'id': '1003', 'name': 'Leicester', 'slug': 'leicester-city'}, 
    {'id': '29', 'name': 'Everton', 'slug': 'fc-everton'}, 
    {'id': '543', 'name': 'Wolves', 'slug': 'wolverhampton-wanderers'}
]

```

```
squad = transfermarkt.squad(281, 2020).get()
squad_stats = transfermarkt.squad_stats(281, 2020).get()
```
---