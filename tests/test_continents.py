import unittest
from unittest.mock import patch
from transfermarkt.endpoints import Continents, Base


class TestContinents(unittest.TestCase):

    def setUp(self):
        self.continents = Continents()

    def test_get(self):
        self.assertEqual(self.continents.get(), ['europa', 'asien', 'amerika', 'afrika'])


if __name__ == '__main__':
    unittest.main()
