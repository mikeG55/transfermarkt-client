import unittest
from unittest.mock import patch
from requests.exceptions import HTTPError
from transfermarkt.endpoints import Base
base = Base()


class TestBase(unittest.TestCase):
    """ Tests for ``Base`` """

    def test_make_request(self):
        url = "https://www.transfermarkt.com/wettbewerbe/europa/wettbewerbe?plus=1"
        fake_text = """<!DOCTYPE html>
                        <head></head>
                        <body></body>
                        </html>"""

        with patch("transfermarkt.endpoints.Base._make_request") as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.text = fake_text

            response = base._make_request(url=url)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.text, fake_text)

    def test_make_request_fails(self):
        pass

    def test_get_response_fails(self):
        # TODO not sure this works
        with patch("transfermarkt.endpoints.Base._get_response") as mock_get:
            mock_get.raise_for_status.side_effect = HTTPError

            base._get_response(url="https://www.transfermarkt.com/")
            self.assertRaises(HTTPError)

    def test_get_response(self):
        url = "https://www.transfermarkt.com/wettbewerbe/europa/wettbewerbe?plus=1"
        fake_text = """<!DOCTYPE html>
                        <head></head>
                        <body></body>
                        </html>"""

        with patch("transfermarkt.endpoints.Base._get_response") as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.text = fake_text

            response = base._get_response(url=url)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.text, fake_text)


if __name__ == "__main__":
    unittest.main()
