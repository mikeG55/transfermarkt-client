import unittest
import bs4
from unittest.mock import patch
from transfermarkt.endpoints import Teams


class TestTeams(unittest.TestCase):
    """ Tests for ``Teams`` """
    def setUp(self):
        column = """<td class="hauptlink no-border-links show-for-small show-for-pad">
                        <a class="vereinprofil_tooltip" 
                        href="/wurzburger-kickers/startseite/verein/1557/saison_id/2020" id="1557">Würzb. Kickers</a>
                    </td>"""
        self.team = bs4.BeautifulSoup(column, 'html.parser')

    def test_get_name(self):
        """ test ``_get_name()`` """
        self.assertEqual(Teams._get_name(self.team), "Würzb. Kickers")

    def test_get_slug(self):
        """ test ``_get_slug()`` """
        self.assertEqual(Teams._get_slug(self.team), "wurzburger-kickers")

    def test_get_unique_identifier(self):
        """ test ``_get_unique_identifier()`` """
        self.assertEqual(Teams._get_unique_identifier(self.team), "1557")

    def test_get_url(self):
        """ test ``get_url()`` """
        self.assertEqual(Teams('GB1', 2019).get_url(),
                         "https://www.transfermarkt.com/competition/startseite/wettbewerb/GB1/saison_id/2019/plus/1")

        self.assertEqual(Teams('', 0000).get_url(),
                         "https://www.transfermarkt.com/competition/startseite/wettbewerb//saison_id/0/plus/1")

    def test_get(self):
        """ test ``get()`` """
        fake_output = [{'id': '281', 'name': 'Man City', 'slug': 'manchester-city'},
                       {'id': '31', 'name': 'Liverpool', 'slug': 'fc-liverpool'},
                       {'id': '148', 'name': 'Spurs', 'slug': 'tottenham-hotspur'},
                       {'id': '631', 'name': 'Chelsea', 'slug': 'fc-chelsea'},
                       {'id': '985', 'name': 'Man Utd', 'slug': 'manchester-united'}]
        with patch("transfermarkt.endpoints.teams.Teams.get") as mock_get:
            mock_get.return_value = fake_output

            response = Teams('GB1', 2019).get()
            self.assertEqual(response, fake_output)


if __name__ == "__main__":
    unittest.main()
