import unittest
from transfermarkt import TransfermarktClient


class TestTransfermarktClient(unittest.TestCase):
    """ Tests  ``TransfermarktClient"""

    def setUp(self):
        self.transfermarkt = TransfermarktClient()

    def test_continents(self):
        self.assertEqual(
            repr(self.transfermarkt.continents()), "<Continents>")

    def test_countries(self):
        self.assertEqual(
            repr(self.transfermarkt.countries(continent="")), "<Countries>")

    def test_competitions(self):
        self.assertEqual(
            repr(self.transfermarkt.competitions(country_id="", season="")), "<Competitions>")

    def test_teams(self):
        self.assertEqual(
            repr(self.transfermarkt.teams(competition_id="", season="")), "<Teams>")

    def test_squad(self):
        self.assertEqual(
            repr(self.transfermarkt.squad(team_id="", season="")), "<Squad>")

    def test_squad_stats(self):
        self.assertEqual(
            repr(self.transfermarkt.squad_stats(team_id="", season="")), "<SquadStats>")



if __name__ == "__main__":
    unittest.main()