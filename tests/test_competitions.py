import unittest
import bs4
from unittest.mock import patch
from transfermarkt.endpoints import Competitions


class TestCompetitions(unittest.TestCase):
    """ Tests for ``Competitions`` """

    def setUp(self) -> None:
        column = """
                        <td class="hauptlink">
                        <table class="inline-table">
                        <tr>
                            <td>
                                <a href="/landespokal-niedersachsen-amateure-/startseite/wettbewerb/Landespokal+Niedersachsen+
                                %28Amateure%29">
                                <img alt="Landespokal Niedersachsen (Amateure)" class="" 
                                src="https://tmssl.akamaized.net/images/logo/tiny/npam.png?lm=1538742317" 
                                title="Landespokal Niedersachsen (Amateure)"/> </a>
                            </td>
                            <td>
                            <a href="/landespokal-niedersachsen-amateure-/startseite/wettbewerb/NPAM" 
                            title="Landespokal Niedersachsen (Amateure)">Niedersachsenpokal (Amateure)</a>
                            </td>
                        </tr>
                        </table>
                        </td>"""
        self.competition = bs4.BeautifulSoup(column, 'html.parser')

    def test_get_name(self):
        """ test ``_get_name()`` """
        self.assertEqual(Competitions._get_name(self.competition), "Niedersachsenpokal (Amateure)")

    def test_get_slug(self):
        """ test ``_get_slug()`` """
        self.assertEqual(Competitions._get_slug(self.competition), "landespokal-niedersachsen-amateure-")

    def test_get_unique_identifier(self):
        """ test ``_get_unique_identifier()`` """
        self.assertEqual(Competitions._get_unique_identifier(self.competition), "NPAM")

    def test_get_url(self):
        """ test ``get_url()`` """
        self.assertEqual(Competitions(00, 00).get_url(),
                         "https://www.transfermarkt.com/wettbewerbe/national/wettbewerbe/0/saison_id/0/plus/1")
        self.assertEqual(Competitions(189, 2019).get_url(),
                         "https://www.transfermarkt.com/wettbewerbe/national/wettbewerbe/189/saison_id/2019/plus/1")

    def test_get(self):
        """ test ``get()`` """
        fake_output = [{'id': 'GB1', 'name': 'Premier League', 'slug': 'premier-league'},
                       {'id': 'GB2', 'name': 'Championship', 'slug': 'championship'},
                       {'id': 'GB3', 'name': 'League One', 'slug': 'league-one'},
                       {'id': 'GB4', 'name': 'League Two', 'slug': 'league-two'},
                       {'id': 'CNAT', 'name': 'National League', 'slug': 'national-league'}]
        with patch("transfermarkt.endpoints.competitions.Competitions.get") as mock_get:
            mock_get.return_value = fake_output

            response = Competitions(189, 2019).get()
            self.assertEqual(response, fake_output)

        fake_output = []
        with patch("transfermarkt.endpoints.competitions.Competitions.get") as mock_get:
            mock_get.return_value = fake_output

            response = Competitions(00, 00).get()
            self.assertEqual(response, fake_output)


if __name__ == "__main__":
    unittest.main()
