import unittest
import bs4
from unittest.mock import patch
from transfermarkt.endpoints import Countries


class TestCountries(unittest.TestCase):
    """ Tests for ``Countries`` """

    def setUp(self) -> None:
        column = """<td class="zentriert">
                        <img alt="Romania" class="flaggenrahmen" 
                            src="https://tmssl.akamaized.net/images/flagge/tiny/140.png?lm=1520611569" 
                            title="Romania"/>
                    </td>"""
        self.country = bs4.BeautifulSoup(column, 'html.parser')

    def test_get_name(self):
        """ test ``_get_name()`` """
        self.assertEqual(Countries._get_name(self.country), "Romania")

    def test_get_unique_identifier(self):
        """ test ``_get_unique_identifier()`` """
        self.assertEqual(Countries._get_unique_identifier(self.country), "140")

    def test_validate_continent(self):
        """ test ``_validate_continent()`` """
        pass

    def test_get_url(self):
        """ test ``get_url()`` """
        self.assertEqual(Countries("europa").get_url(),
                         "https://www.transfermarkt.com/wettbewerbe/europa/wettbewerbe?plus=1")
        self.assertEqual(Countries("asien").get_url(),
                         "https://www.transfermarkt.com/wettbewerbe/asien/wettbewerbe?plus=1")
        self.assertEqual(Countries("amerika").get_url(),
                         "https://www.transfermarkt.com/wettbewerbe/amerika/wettbewerbe?plus=1")
        self.assertEqual(Countries("afrika").get_url(),
                         "https://www.transfermarkt.com/wettbewerbe/afrika/wettbewerbe?plus=1")

    def test_get(self):
        """ test ``get()`` """
        fake_output = [{'id': '189', 'name': 'England'}, {'id': '75', 'name': 'Italy'},
                       {'id': '157', 'name': 'Spain'}, {'id': '40', 'name': 'Germany'},
                       {'id': '50', 'name': 'France'}]
        with patch("transfermarkt.endpoints.countries.Countries.get") as mock_get:
            mock_get.return_value = fake_output

            response = Countries("europa").get()
            self.assertEqual(response, fake_output)

        fake_output = []
        with patch("transfermarkt.endpoints.countries.Countries.get") as mock_get:
            mock_get.return_value = fake_output

            response = Countries("europa").get()
            self.assertEqual(response, fake_output)


if __name__ == "__main__":
    unittest.main()
