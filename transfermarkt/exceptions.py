class InvalidContinent(Exception):
    """ Invalid Continent """

    def __init__(self, *args):
        if args:
            self.message = f"'{args[0]}' is not a valid continent"
        else:
            self.message = "The value passed to ``continent`` is not valid"

    def __str__(self) -> str:
        return self.message

class InvalidCompetition(Exception):
    """ Invalid Competition """

    def __init__(self, country_id, season):
        if country_id:
            self.message = f"{country_id} is not a valid country_id"
        if season:
            self.message = f"{season} is not a valid season"
        if country_id and season:
            self.message = f"country_id {country_id} and season {season} are not valid"
        else:
            self.message = "The values passed to ``competition`` are not valid"

    def __str__(self) -> str:
        return self.message