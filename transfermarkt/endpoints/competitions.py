import bs4
from .base import Base
from ..exceptions import InvalidCompetition


class Competitions(Base):
    """

    """
    def __init__(self, country_id: int, season: int):
        """

        :param country_id: int
        :param season: int
        """
        self.country_id = country_id
        self.season = season
        super().__init__()
        self.url = f"{self.base_url}wettbewerbe/national/wettbewerbe/{self.country_id}/saison_id/{self.season}/plus/1"

    @staticmethod
    def _get_name(competition: bs4.element.Tag) -> str:
        """
        Get competition name

        :param competition: bs4 object
        :return: str
        """
        # data is in the second a tag.
        return competition.find_all('a')[-1].text

    @staticmethod
    def _get_slug(competition: bs4.element.Tag) -> str:
        """
        Get competition slug name

        :param competition: bs4 object
        :return: str
        """
        # data is in the second a tag.
        return competition.find_all('a')[-1]['href'].split("/")[1]

    @staticmethod
    def _get_unique_identifier(competition: bs4.element.Tag) -> str:
        """
        Get competition id

        :param competition: bs4 object
        :return: str
        """
        # data is in the second a tag.
        href = competition.find_all('a')[-1]['href']
        if '/saison_id' in href:
            href = href.split('/saison_id')[0]

        return href.split("/")[-1]

    def get_url(self) -> str:
        """

        :return: str
        """
        return self.url

    def get(self) -> list:
        """

        :return:
        """
        output = []
        response = self._get_response(self.url)
        if len(response.text) == 0:
            return output

        soup = bs4.BeautifulSoup(response.text, 'html.parser')
        content_div = soup.find("div", id="yw1")
        if content_div is None:
            return output

        squad_table = content_div.find("table", {"class": "items"})
        header_row = squad_table.thead.tr
        headers = header_row.find_all('th')
        headers = [h.text.lower() for h in headers]
        # ['competition', 'clubs', 'player', 'avg. age', 'foreigners', 'goals per match', 'forum', 'total value']

        content_rows = squad_table.tbody
        content_rows = content_rows.find_all('tr', recursive=False)
        for row in content_rows:
            # only want to process the rows with a class. Most tier labelled rows do not have a class.
            # we do not want these rows at the moment.
            if not row.has_attr('class'):
                continue
            details = {}
            columns = row.find_all('td', recursive=False)
            row_dict = dict(zip(headers, columns))
            details['id'] = self._get_unique_identifier(row_dict['competition'])
            details['name'] = self._get_name(row_dict['competition'])
            details['slug'] = self._get_slug(row_dict['competition'])
            output.append(details)

        return output
