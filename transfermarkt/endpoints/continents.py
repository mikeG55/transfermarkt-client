from .base import Base


class Continents(Base):
    """

    """
    @staticmethod
    def get():
        """

        :return:
        """
        return ['europa', 'asien', 'amerika', 'afrika']
