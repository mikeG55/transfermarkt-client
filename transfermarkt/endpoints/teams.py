import bs4
from .base import Base


class Teams(Base):
    """

    """
    def __init__(self, competition_id, season):
        """

        """
        self.competition_id = competition_id
        self.season = season
        super().__init__()
        self.url = f"{self.base_url}competition/startseite/wettbewerb/{competition_id}/saison_id/{season}/plus/1"

    @staticmethod
    def _get_name(team: bs4.element.Tag) -> str:
        """
        Get team name

        :param team: bs4 object
        :return: str
        """
        return team.a.text

    @staticmethod
    def _get_slug(team: bs4.element.Tag) -> str:
        """
        Get team slug name

        :param team: bs4 object
        :return: str
        """
        return team.a['href'].split("/")[1]

    @staticmethod
    def _get_unique_identifier(team: bs4.element.Tag) -> int:
        """
        Get team id

        :param team: bs4 object
        :return: str
        """
        return team.a['id']

    def get_url(self) -> str:
        return self.url

    def get(self) -> list:
        """

        :return: list
        """
        output = []
        response = self._get_response(url=self.url)
        if len(response.text) == 0:
            return output

        soup = bs4.BeautifulSoup(response.text, 'html.parser')
        content_div = soup.find("div", id="yw1")
        if content_div is None:
            return output

        squad_table = content_div.find("table", {"class": "items"})
        header_row = squad_table.thead.tr
        headers = header_row.find_all('th')
        headers = [h.text for h in headers]
        # TODO better check than this?
        # Putting a bad competition id can sometimes bring back the wrong page
        # example this https://www.transfermarkt.com/competition/startseite/wettbewerb/1/saison_id/2020/plus/1
        # with competition_id as 1 brings back page https://www.transfermarkt.com/wettbewerbe/national
        # if 'name' not in headers:
        #     return {"message": 'Not Found',
        #             "status_code": 404,
        #             "data": output}

        # ['club', 'name', 'name', 'Squad', 'ø age', 'Foreigners', 'ø age', 'Foreigners', 'Total market value',
        # 'ø market value', 'Total MV', 'ø MV']

        content_rows = squad_table.tbody
        content_rows = content_rows.find_all('tr', recursive=False)
        for row in content_rows:
            details = {}
            columns = row.find_all('td', recursive=False)
            row_dict = dict(zip(headers, columns))
            # TODO use main name not shortened name
            details['id'] = self._get_unique_identifier(row_dict['name'])
            details['name'] = self._get_name(row_dict['name'])
            details['slug'] = self._get_slug(row_dict['name'])
            output.append(details)

        return output
