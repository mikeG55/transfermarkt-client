from bs4 import BeautifulSoup
from .base import Base


class SquadStats(Base):
    """

    """
    def __init__(self, team_id, season):
        """

        """
        self.team_id = team_id
        self.season = season
        super().__init__()
        self.url = f"{self.base_url}team/leistungsdaten/verein/{self.team_id}/reldata/%26{self.season}/plus/1"

    def get(self):
        """

        :return:
        """
        output = []
        response = self._get_response(url=self.url)

        soup = BeautifulSoup(response.text, 'html.parser')
        content_div = soup.find("div", id="yw1")
        # if content_div is None or content_div.span.text == "No information":
        # #     return

        squad_table = content_div.find("table", {"class": "items"})
        header_row = squad_table.thead.tr
        headers = header_row.find_all('th')
        headers = [h.span['title'] if h.span else h.text for h in headers]
        # ['#', 'player', 'Age', 'Nat.', 'In squad', 'Appearances', 'Tore', 'Assists', 'Yellow cards',
        # 'Second yellow cards', 'Red cards', 'Substitutions on', 'Substitutions off', 'PPG', 'Minutes played']

        content_rows = squad_table.tbody
        content_rows = content_rows.find_all('tr', recursive=False)
        for row in content_rows:
            details = {}
            columns = row.find_all('td', recursive=False)
            row_dict = dict(zip(headers, columns))
            details['name'] = player.name(row_dict['player'])
            details['in_squad'] = player.in_squad(row_dict['In squad'])
            details['appearances'] = player.appearances(row_dict['Appearances'])
            details['goals'] = player.goals(row_dict['Tore'])
            details['assists'] = player.assists(row_dict['Assists'])
            details['yellow_cards'] = player.yellow_cards(row_dict['Yellow cards'])
            details['second_yellow_cards'] = player.second_yellow_cards(row_dict['Second yellow cards'])
            details['red_cards'] = player.red_cards(row_dict['Red cards'])
            details['subbed_on'] = player.subbed_on(row_dict['Substitutions on'])
            details['subbed_off'] = player.subbed_off(row_dict['Substitutions off'])
            details['ppg'] = player.ppg(row_dict['PPG'])
            details['minutes_played'] = player.minutes_played(row_dict['Minutes played'])
            output.append(details)

        return output

