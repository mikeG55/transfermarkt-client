import bs4
from .base import Base
from .continents import Continents
from ..exceptions import (
    InvalidContinent
)


class Countries(Base):
    """

    """
    def __init__(self, continent):
        """

        """
        self.continent = continent
        self._validate_continent()
        super().__init__()
        self.url = f"{self.base_url}wettbewerbe/{continent}/wettbewerbe?plus=1"

    @staticmethod
    def _get_name(country: bs4.element.Tag) -> str:
        """
        Get country name

        :param country: bs4 object
        :return: str
        """
        return country.img['title']

    @staticmethod
    def _get_unique_identifier(country: bs4.element.Tag) -> int:
        """
        Get country id

        :param country: bs4 object
        :return: str
        """
        return country.img['src'].split("/")[-1].split(".png")[0]

    def _validate_continent(self):
        if self.continent not in Continents.get():
            raise InvalidContinent(self.continent)

    def get_url(self) -> str:
        return self.url

    def get(self) -> list:
        """

        :return: list
        """
        output = []
        response = self._get_response(url=self.url)
        if len(response.text) == 0:
            return output

        soup = bs4.BeautifulSoup(response.text, 'html.parser')
        content_div = soup.find("div", id="yw1")
        if content_div is None:
            return output

        squad_table = content_div.find("table", {"class": "items"})
        header_row = squad_table.thead.tr
        headers = header_row.find_all('th')
        headers = [h.text for h in headers]
        # ['competition', 'country', 'Clubs', 'player', 'Avg. age', 'Foreigners', 'Game ratio of foreign players',
        # 'Goals per match', 'Forum', 'Average market value', 'Total value']

        content_rows = squad_table.tbody
        content_rows = content_rows.find_all('tr', recursive=False)
        for row in content_rows:
            if not row.has_attr('class'):
                continue

            details = {}
            columns = row.find_all('td', recursive=False)
            row_dict = dict(zip(headers, columns))
            details['id'] = self._get_unique_identifier(row_dict['country'])
            details['name'] = self._get_name(row_dict['country'])
            output.append(details)

        return output
