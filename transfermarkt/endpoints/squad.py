from bs4 import BeautifulSoup
from .base import Base


class Squad(Base):
    """

    """
    def __init__(self, team_id, season):
        """

        """
        self.team_id = team_id
        self.season = season
        super().__init__()
        self.url = f"{self.base_url}team/kader/verein/{self.team_id}/saison_id/{self.season}/plus/1"

    def get(self):
        """

        :return:
        """
        output = []
        response = self._get_response(url=self.url)

        soup = BeautifulSoup(response.text, 'html.parser')
        content_div = soup.find("div", id="yw1")
        squad_table = content_div.find("table", {"class": "items"})
        header_row = squad_table.thead.tr
        headers = header_row.find_all('th')
        headers = [h.text for h in headers]
        # ['#', 'player', 'Date of birth / Age', 'Nat.', 'Height', 'Foot', 'Joined', 'Signed from', 'Contract expires',
        # 'Market value']

        content_rows = squad_table.tbody
        content_rows = content_rows.find_all('tr', recursive=False)
        for row in content_rows:
            details = {}
            columns = row.find_all('td', recursive=False)
            row_dict = dict(zip(headers, columns))
            details['name'] = player.name(row_dict['player'])
            details['dob'], details['age'] = player.dob(row_dict['Date of birth / Age'])
            # TODO deal with dual nationality.
            details['nationality'] = player.nationality(row_dict['Nat.'])
            # TODO if historic season parser current club. Also player age relevant to season.
            details['height'] = player.height(row_dict['Height'])
            details['foot'] = player.foot(row_dict['Foot'])
            details['joined'] = player.joined(row_dict['Joined'])
            details['signed_from'] = player.signed_from(row_dict['Signed from'])
            details['contract_expires'] = player.contract_expires(row_dict['Contract expires'])
            details['market_value'] = player.market_value(row_dict['Market value'])
            output.append(details)

        return output

