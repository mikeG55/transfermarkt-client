import requests


class Base:
    """

    """
    def __init__(self):
        """

        """
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36'}
        self.base_url = "https://www.transfermarkt.com/"

    def __repr__(self):
        return "<%s>" % self.__class__.__name__

    def _make_request(self, *args, **kwargs):
        """
        """
        resp = requests.get(headers=self.headers, allow_redirects=False, *args, **kwargs)
        resp.raise_for_status()

        return resp

    def _get_response(self, url: str) -> requests.models.Response:
        """

        :param url: str
        :return: requests.models.Response
        """
        return self._make_request(url=url)
