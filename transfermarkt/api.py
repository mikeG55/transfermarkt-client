from .endpoints import (
    Continents,
    Countries,
    Competitions,
    Teams,
    Squad,
    SquadStats
)


class TransfermarktClient:
    """

    """

    def __init__(self):
        """

        """

    @staticmethod
    def continents():
        """
        """
        return Continents()

    @staticmethod
    def countries(continent: str):
        """
        """
        return Countries(continent=continent)

    @staticmethod
    def competitions(country_id: int, season: int):
        """
        """
        return Competitions(country_id=country_id, season=season)

    @staticmethod
    def teams(competition_id: str, season: int):
        """
        """
        return Teams(competition_id=competition_id, season=season)

    @staticmethod
    def squad(team_id: int, season: int):
        """
        """
        return Squad(team_id=team_id, season=season)

    @staticmethod
    def squad_stats(team_id: int, season: int):
        """
        """
        return SquadStats(team_id=team_id, season=season)
